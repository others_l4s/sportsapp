﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.Utility
{
    public static class Extensions
    {
        public static ApiResponse ToApiResponse(this ValidationResult result)
        {
            var apiResponse = new ApiResponse();
            foreach (var item in result.Errors)
            {
                apiResponse.AddError(item.ErrorMessage);
            }
            return apiResponse;
        }
    }
}
