﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.Utility
{
    public class ApiResponse
    {
        public ApiResponse()
        {
            this.Errors = new List<string>();
        }

        public ApiResponse(string errorCode)
        {
            this.Errors.Add(errorCode);
        }

        public List<string> Errors { get; set; }

        public bool IsValid
        {
            get
            {
                return this.Errors.Count < 1;
            }
        }

        public object Data { get; set; }

        public void AddError(string error)
        {
            this.Errors.Add(error);
        }
    }
}
