﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using SATSports.API.APIModel;
using SATSports.API.Data;

namespace SATSports.API.Repository
{
    public interface IDatabaseRepository
    {
        long SaveUser(MobileAppLoginModel registerUserModel);
        long CheckUserLogin(LoginModel model);
        User GetUserDetails(long userId);
        long CheckAdminLogin(LoginModel model);
        List<User> GetAllUsers();
        List<string> GetNewsTickers();
        void SaveNewsTicker(NewsTickerModel newsTickerModel);
        void UpdateNewsTicker(NewsTickerModel newsTickerModel);
        void DeleteNewsTicker(NewsTickerModel newsTickerModel);
        bool SaveUserFCMToken(FCMTokenModel fCMTokenModel);
        void SaveUserFeedback(UserFeedbackModel model);
    }
}
