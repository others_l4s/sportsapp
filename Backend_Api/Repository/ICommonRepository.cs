﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using SATSports.API.APIModel;
using SATSports.API.Data;
using SATSports.API.GridOperations;

namespace SATSports.API.Repository
{
    public interface ICommonRepository<T1> where T1 : class
    {
        PagedResult<T1> GetAll(GridData gridData);

        DataTable GetAll();
    }
}
