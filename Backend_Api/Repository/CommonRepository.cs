﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using SATSports.API.APIModel;
using SATSports.API.Data;
using SATSports.API.GridOperations;

namespace SATSports.API.Repository
{
	public class CommonRepository<T1> : ICommonRepository<T1> where T1 : class
	{
		private readonly string connectionString = string.Empty;
		public SqlConnection con;
		public CommonRepository(IConfiguration configuration)
		{
			connectionString = configuration.GetConnectionString("DefaultConnection");
		}
		private void Connection()
		{
			con = new SqlConnection(connectionString);
		}

		public PagedResult<T1> GetAll(GridData gridData)
		{
			var gridParams = new GridFunctions(gridData).SetGridParams();

			var rawQuery = @"SELECT {0}, ROW_NUMBER() OVER(ORDER BY {1}) RowNumber
                                    Into #Results
                                    FROM {2} where {3} ORDER BY {1} 
                                Select @TotalRows = Count(0) from #Results
                                SELECT  *
                                FROM    #Results
                                WHERE   RowNumber  BETWEEN (@Skip + 1)
                                        AND (@Skip + @PageSize)
                                Drop Table #Results";
			//var sqlParameters = new List<SqlParameter>();

			var skip = 0;
			if (gridParams.Page > 0)
			{
				skip = gridParams.RecordPerPage * gridParams.Page;
			}

			DynamicParameters parameter = new DynamicParameters();

			parameter.Add("Skip", skip, DbType.Int32, ParameterDirection.Input);
			parameter.Add("PageSize", gridParams.RecordPerPage, DbType.Int32, ParameterDirection.Input);
			parameter.Add("TotalRows", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);


			//sqlParameters.Add(new SqlParameter("Skip", skip));
			//sqlParameters.Add(new SqlParameter("PageSize", gridParams.RecordPerPage));
			//var totalRowOutput = new SqlParameter
			//{
			//    ParameterName = "TotalRows",
			//    SqlDbType = SqlDbType.Int,
			//    Value = 0,
			//    Direction = ParameterDirection.Output
			//};
			//sqlParameters.Add(totalRowOutput);

			if (!string.IsNullOrEmpty(gridParams.SearchText))
			{
				parameter.Add("Search", gridParams.SearchText, DbType.String, ParameterDirection.Input);

				//sqlParameters.Add(new SqlParameter("Search", gridParams.SearchText));
			}
			var sortExpression = $"{gridParams.SortColumn} {gridParams.SortOrder}";
			var columnName = string.IsNullOrWhiteSpace(gridParams.ColumnName) ? " * " : gridParams.ColumnName;
			var query = string.Format(rawQuery, columnName, sortExpression, gridParams.TableName, gridParams.WhereClause);

			try
			{
				Connection();
				con.Open();
				var results = SqlMapper.Query<T1>(
					con,
					query, parameter);
				con.Close();

				var result = new PagedResult<T1>();
				int rowCount = parameter.Get<int>("TotalRows");
				result.TotalRecords = rowCount;
				result.Data = results.ToList();
				return result;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				con.Close();
			}
			return null;
		}

		public DataTable GetAll()
		{
			var dt = new DataTable();
			try
			{
				Connection();
				con.Open();
				var results = SqlMapper.ExecuteReader(
					con,
					"Select MobileNo from MobileAppUsers");
				dt.Load(results);
				con.Close();
				return dt;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				con.Close();
			}
			return dt;
		}
	}
}
