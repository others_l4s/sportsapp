﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using SATSports.API.APIModel;
using SATSports.API.Data;

namespace SATSports.API.Repository
{
    public class DatabaseRepository : IDatabaseRepository
    {
        private readonly string connectionString = string.Empty;
        public SqlConnection con;
        public DatabaseRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        private void Connection()
        {
            con = new SqlConnection(connectionString);
        }

        public long SaveUser(MobileAppLoginModel registerUserModel)
        {
            try
            {
                var existingUserId = ExistingUser(registerUserModel);
                if (existingUserId > 0)
                    return existingUserId;

                var user = new MobileAppUsers()
                {
                    MobileNo = registerUserModel.MobileNo,
                    CreatedOn = DateTime.Now,
                };
                var userId = con.Insert<MobileAppUsers>(user);
                return userId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }

        public bool SaveUserFCMToken(FCMTokenModel fCMTokenModel)
        {
            try
            {
                Connection();
                con.Open();
                MobileAppUsers user = SqlMapper.QueryFirstOrDefault<MobileAppUsers>(
                    con,
                    @"SELECT * From MobileAppUsers WHERE MobileNo = @MobileNo",
                    new { fCMTokenModel.MobileNo});
                if(user != null)
                {
                    con.Update<MobileAppUsers>(user);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }
        
        public long ExistingUser(MobileAppLoginModel registerUserModel)
        {
            try
            {
                Connection();
                con.Open();
                MobileAppUsers user = SqlMapper.QueryFirstOrDefault<MobileAppUsers>(
                    con,
                    @"SELECT Top 1 MobileAppUserId From MobileAppUsers WHERE MobileNo = @MobileNo",
                    new
                    {
                       registerUserModel.MobileNo,
                    });
                con.Close();
                if (user != null)
                    return user.MobileAppUserId;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return 0;
        }

        public long CheckUserLogin(LoginModel model)
        {
            try
            {
                Connection();
                con.Open();
                User user = SqlMapper.QueryFirstOrDefault<User>(
                    con,
                    @"SELECT Top 1 UserId From Users WHERE Email = @emailId AND Password = @password AND Role = 1",
                    new
                    {
                        emailId = model.Email,
                        password = model.Password
                    });
                con.Close();
                if (user != null)
                    return user.UserId;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return 0;
        }

        public long CheckAdminLogin(LoginModel model)
        {
            try
            {
                Connection();
                con.Open();
                User user = SqlMapper.QueryFirstOrDefault<User>(
                    con,
                    @"SELECT Top 1 UserId From Users WHERE Email = @emailId AND Password = @password AND Role = 0",
                    new
                    {
                        emailId = model.Email,
                        password = model.Password
                    });
                con.Close();
                if (user != null)
                    return user.UserId;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return 0;
        }

        public User GetUserDetails(long userId)
        {
            try
            {
                Connection();
                con.Open();
                User user = SqlMapper.QueryFirstOrDefault<User>(
                    con,
                    @"SELECT * From Users WHERE UserId = @userId",
                    new { userId });
                con.Close();
                return user;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public List<User> GetAllUsers()
        {
            try
            {
                Connection();
                con.Open();
                var user = SqlMapper.Query<User>(
                    con,
                    @"SELECT * From Users");
                con.Close();
                return user.ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public List<string> GetNewsTickers()
        {
            try
            {
                Connection();
                con.Open();
                var newsTickers = SqlMapper.Query<string>(
                    con,
                    @"SELECT title From NewsTicker WHERE IsActive = 1");
                con.Close();
                return newsTickers.ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public void SaveNewsTicker(NewsTickerModel newsTickerModel)
        {
            try
            {
                Connection();
                con.Open();
                NewsTicker newsTicker = SqlMapper.QueryFirstOrDefault<NewsTicker>(
                  con,
                  @"SELECT Top 1 NewsTickerId From NewsTicker WHERE Title = @title",
                  new
                  {
                      title = newsTickerModel.Title
                  });

                if (newsTicker != null)
                    return;

                newsTicker = new NewsTicker()
                {
                    Title = newsTickerModel.Title,
                    IsActive = newsTickerModel.IsActive,
                    CreatedOn = DateTime.Now,
                    CreatedBy = 1
                };
                con.Insert<NewsTicker>(newsTicker);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNewsTicker(NewsTickerModel newsTickerModel)
        {
            try
            {
                Connection();
                con.Open();
                var existingNewsTicker = SqlMapper.QueryFirstOrDefault<NewsTicker>(
                  con,
                  @"SELECT * From NewsTicker WHERE NewsTickerId = @newsTickerId",
                  new
                  {
                      newsTickerId = newsTickerModel.NewsTickerId
                  });

                if (existingNewsTicker == null)
                {
                    return;
                }

                // duplictaion check
                var duplicateNewsTicker = SqlMapper.QueryFirstOrDefault<NewsTicker>(
                  con,
                  @"SELECT Top 1 NewsTickerId From NewsTicker WHERE Title = @title and NewsTickerId <> @newsTickerId",
                  new
                  {
                      title = newsTickerModel.Title,
                      newsTickerId = newsTickerModel.NewsTickerId
                  });

                if (duplicateNewsTicker != null)
                    return;

                existingNewsTicker = new NewsTicker()
                {
                    NewsTickerId = newsTickerModel.NewsTickerId,
                    Title = newsTickerModel.Title,
                    IsActive = newsTickerModel.IsActive,
                    CreatedOn = existingNewsTicker.CreatedOn,
                    CreatedBy = existingNewsTicker.CreatedBy,
                    UpdatedOn = DateTime.Now,
                    UpdatedBy = 1
                };
                con.Update<NewsTicker>(existingNewsTicker);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteNewsTicker(NewsTickerModel newsTickerModel)
        {
            try
            {
                Connection();
                con.Open();
                NewsTicker newsTicker = SqlMapper.QueryFirstOrDefault<NewsTicker>(
                  con,
                  @"Select * From NewsTicker WHERE NewsTickerId = @newsTickerId",
                  new
                  {
                      newsTickerId = newsTickerModel.NewsTickerId
                  });

                if (newsTicker == null)
                    return;

                con.Delete<NewsTicker>(newsTicker);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveUserFeedback(UserFeedbackModel model)
        {
            try
            {
                Connection();
                con.Open();

                MobileAppUsers user = SqlMapper.QueryFirstOrDefault<MobileAppUsers>(
                    con,
                    @"SELECT Top 1 MobileAppUserId From MobileAppUsers WHERE MobileNo = @MobileNo",
                    new
                    {
                        model.MobileNo,
                    });

                var feedback = new UserFeedback()
                {
                    Title = model.Title,
                    Description = model.Description,
                    CreatedOn = DateTime.Now,
                    CreatedBy = user.MobileAppUserId
                };
                con.Insert<UserFeedback>(feedback);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
