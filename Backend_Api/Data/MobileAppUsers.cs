﻿using Dapper.Contrib.Extensions;
using System;

namespace SATSports.API.Data
{
    [Table("MobileAppUsers")]
    public class MobileAppUsers
    {
        [Key]
        public long MobileAppUserId { get; set; }
        public string MobileNo { get; set; }
        public string FCMToken { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
