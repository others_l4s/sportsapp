﻿using Dapper.Contrib.Extensions;
using System;

namespace SATSports.API.Data
{
    [Table("UserFeedback")]
    public class UserFeedback
    {
        [Key]
        public long UserFeedbackId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
    }
}
