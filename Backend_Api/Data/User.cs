﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.Data
{
    [Table("Users")]
    public class User
    {
        [Key]
        public long UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string ProfileImage { get; set; }
        public string Password { get; set; }
        public Int16 Role { get; set; }
        public Int16 RegistrationType { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
