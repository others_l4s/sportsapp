﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.Data
{
    [Table("NewsTicker")]
    public class NewsTicker
    {
        [Key]
        public long NewsTickerId { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
    }
}
