﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { Feedback, Page } from '@app/_models';
import { FeedBackService } from '@app/_services';
import { ToastrService } from 'ngx-toastr';

@Component({ templateUrl: 'feedback.component.html' })
export class FeedBackComponent {
    loading = false;
    feedBacks: [];
    sort = { sortColumn: "Title", sortDir: "Asc" };
    page = new Page();
    constructor(private feedBackService: FeedBackService, private toastr: ToastrService, private router: Router) {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.router.navigate(['/user-feedbacks']);
    }
    ngOnInit() {
        this.setPage({ offset: 0 });
    }
    setPage(pageInfo) {
        this.loading = true;
        this.page.pageNumber = pageInfo.offset;
        this.getData();
    }
    getData() {
        this.feedBackService.getAll({ page: this.page.pageNumber, recordsPerPage: this.page.size, sortColumn: this.sort.sortColumn, sortOrder: this.sort.sortDir }).subscribe(data => {
            this.loading = false;
            this.feedBacks = data.data.data;
            this.page.totalElements = data.data.totalRecords;
        });
    }
    onSort(event) {
        const sort = event.sorts[0];
        this.sort.sortColumn = sort.prop;
        this.sort.sortDir = sort.dir;
        this.getData();
    }

}