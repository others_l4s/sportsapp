﻿export class Feedback {
    mobileNo: string;
    title: string;
    description: string;
}