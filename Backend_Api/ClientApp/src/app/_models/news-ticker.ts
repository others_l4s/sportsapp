export class NewsTicker {
  newsTickerId: number;
  title: string;
  isActive: boolean;
}