﻿export class User {
    userId: number;
    emailId: string;
    token?: string;
}