import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from '@environments/environment';
import { User } from '@app/_models';
import { Observable , of} from 'rxjs'

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll(tableparams) {
        const params = new HttpParams().set('page', tableparams.page).
        set('recordsPerPage', tableparams.recordsPerPage)
        .set('sortColumn', tableparams.sortColumn)
        .set('sortOrder', tableparams.sortOrder)
        .set('filters', JSON.stringify(tableparams.filters));
        return this.http.get<any>(`${environment.apiUrl}/admin/getallmobileappusers`, { params: params });
    }

  exportUsers(): Observable<Blob> {
    return this.http.get<any>(`${environment.apiUrl}/admin/exportusers`, { responseType: 'blob' as 'json'});
    }
}
