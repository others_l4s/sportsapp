import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class NewsTickerService {
  constructor(private http: HttpClient) { }

  getAll(tableparams) {
    const params = new HttpParams().set('page', tableparams.page).set('recordsPerPage', tableparams.recordsPerPage).set('sortColumn', tableparams.sortColumn).set('sortOrder', tableparams.sortOrder);
    return this.http.get<any>(`${environment.apiUrl}/data/getAllNewsTickers`, { params: params });
  }

  addNewsTicker(Title: string, IsActive: boolean) {
    return this.http.post<any>(`${environment.apiUrl}/data/addNewsTicker`, { Title, IsActive })
      .pipe(map(response => {
        return response;
      }));
  }

  updateNewsTicker(NewsTickerId: number, Title: string, IsActive: boolean) {
    return this.http.post<any>(`${environment.apiUrl}/data/updateNewsTicker`, { NewsTickerId, Title, IsActive })
      .pipe(map(response => {
        return response;
      }));
  }

  deleteNewsTicker(NewsTickerId: number, Title: string, IsActive: boolean) {
    return this.http.post<any>(`${environment.apiUrl}/data/deleteNewsTicker`, { NewsTickerId, Title, IsActive })
      .pipe(map(response => {
        return response;
      }));
  }
}