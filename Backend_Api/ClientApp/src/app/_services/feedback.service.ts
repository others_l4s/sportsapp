﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Feedback } from '@app/_models';


@Injectable({ providedIn: 'root' })
export class FeedBackService {
    constructor(private http: HttpClient) { }

    getAll(tableparams) {
        const params = new HttpParams().set('page', tableparams.page).set('recordsPerPage', tableparams.recordsPerPage).set('sortColumn', tableparams.sortColumn).set('sortOrder', tableparams.sortOrder);
        return this.http.get<any>(`${environment.apiUrl}/data/getalluserfeedBack`, { params: params });
    }

}