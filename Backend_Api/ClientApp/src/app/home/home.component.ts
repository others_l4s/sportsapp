﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User, Page } from '@app/_models';
import { UserService, AuthenticationService } from '@app/_services';
import { ToastrService } from 'ngx-toastr';

@Component({
    templateUrl: 'home.component.html',
})
export class HomeComponent implements OnInit {
    loading = false;
    users: [];
    sort = { sortColumn: "CreatedOn", sortDir: "Desc" };
    page = new Page();
    public fromdate : string;
    public todate : string;

    constructor(private userService: UserService, private toastr: ToastrService) {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.fromdate = null;
        this.todate = null;
    }

    ngOnInit() {
        this.setPage({ offset: 0 });
    }

    setPage(pageInfo) {
        this.loading = true;
        this.page.pageNumber = pageInfo.offset;
        this.getData();
    }

    getData() {
        var filters = [];
        if (this.fromdate && this.todate) {           
            filters.push({
                fieldName: "CreatedOn",
                fieldValue: this.fromdate + "," + this.todate,
                type: "daterange"
            })
        }
        this.userService.getAll({ page: this.page.pageNumber, recordsPerPage: this.page.size, sortColumn: this.sort.sortColumn, sortOrder: this.sort.sortDir, filters: filters }).subscribe(data => {
            this.loading = false;
            this.users = data.data.data;
            this.page.totalElements = data.data.totalRecords;
        });
    }

    filterDate(){
        this.setPage({ offset: 0 });
    }

    clearFilters(){
        this.fromdate = null;
        this.todate = null;
        this.setPage({ offset: 0 });
    }

    onSort(event) {
        const sort = event.sorts[0];
        this.sort.sortColumn = sort.prop;
        this.sort.sortDir = sort.dir;
        this.getData();
    }

    exportData() {
        this.userService.exportUsers().subscribe(data => {
            this.downloadFile(data);
        }), error => {
            this.toastr.error('Export file error. Please try again later.');
        };
    }

    downloadFile(data) {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        const url = window.URL.createObjectURL(blob);
        //window.open(url);
        var a = document.createElement("a");
        a.href = url;
        a.download = "Userlist.xlsx";
        a.click();
        this.toastr.success('File downloaded successfully.');
    }
}