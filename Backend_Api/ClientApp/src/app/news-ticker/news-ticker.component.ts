import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons , NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';

import { Page } from '@app/_models';
import { NewsTickerService } from '@app/_services';

@Component({ templateUrl: 'news-ticker.component.html' })
export class NewsTickerComponent implements OnInit {
  newsTickerForm: FormGroup;
  modalReference: NgbModalRef;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  newsTickers: [];
  sort = { sortColumn: "Title", sortDir: "Asc" };
  page = new Page();
  closeResult: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private newsTickerService: NewsTickerService,
    private modalService: NgbModal
  ) {
    this.page.pageNumber = 0;
    this.page.size = 10;
    this.router.navigate(['/news-ticker']);
  }

  ngOnInit() {
    this.setPage({ offset: 0 });

    this.newsTickerForm = this.formBuilder.group({
      newsTickerId: [0],
      title: ['', Validators.required],
      isActive: [true]
    });

    //return url to news ticker listing page
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/news-ticker-list';
  }

  // convenience getter for easy access to form fields
  get f() { return this.newsTickerForm.controls; }
  open(content, data) {
    if (data != null) {
      this.newsTickerForm = this.formBuilder.group({
        newsTickerId: data.newsTickerId,
        title: data.title,
        isActive: data.isActive
      });
    }
    this.modalReference = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }

  openDeleteConfirmation(content, data) {
    if (data != null) {
      this.newsTickerForm = this.formBuilder.group({
        newsTickerId: data.newsTickerId,
        title: data.title,
        isActive: data.isActive
      });
    }
    this.modalReference = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }

  setPage(pageInfo) {
    this.loading = true;
    this.page.pageNumber = pageInfo.offset;
    this.getData();
  }
  getData() {
    this.newsTickerService.getAll({ page: this.page.pageNumber, recordsPerPage: this.page.size, sortColumn: this.sort.sortColumn, sortOrder: this.sort.sortDir }).subscribe(data => {
      this.loading = false;
      this.newsTickers = data.data.data;
      this.page.totalElements = data.data.totalRecords;
    });
  }
  onSort(event) {
    const sort = event.sorts[0];
    this.sort.sortColumn = sort.prop;
    this.sort.sortDir = sort.dir;
    this.getData();
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.newsTickerForm.invalid) {
      return;
    }

    this.loading = true;
    if (this.f.newsTickerId.value > 0) {
      this.newsTickerService.updateNewsTicker(this.f.newsTickerId.value, this.f.title.value, this.f.isActive.value)
        .pipe(first())
        .subscribe(
          data => {
            this.modalReference.close();
            this.loading = false;
            this.getData();
          },
          error => {
            this.error = error;
            this.loading = false;
          });
    } else {
      this.newsTickerService.addNewsTicker(this.f.title.value, this.f.isActive.value)
        .pipe(first())
        .subscribe(
          data => {
            this.modalReference.close();
            this.loading = false;
            this.getData();
          },
          error => {
            this.error = error;
            this.loading = false;
          });
    }
  }
  onDeleteConfirmation() {
    this.loading = true;
    this.newsTickerService.deleteNewsTicker(this.f.newsTickerId.value, this.f.title.value, this.f.isActive.value)
      .pipe(first())
      .subscribe(
        data => {
          this.modalReference.close();
          this.loading = false;
          this.getData();
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}
