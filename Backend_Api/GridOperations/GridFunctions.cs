﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.GridOperations
{
    public class GridFunctions
    {
        private readonly GridData _gridData;
        public GridFunctions(GridData data)
        {
            _gridData = data;
        }

        public GridParams SetGridParams()
        {
            var gridParams = new GridParams()
            {
                Page = _gridData.Page,
                RecordPerPage = _gridData.RecordsPerPage,
                SortColumn = _gridData.SortColumn,
                SortOrder = _gridData.SortOrder,
                SearchText = _gridData.SearchText,
                Mode = _gridData.Mode,
                Filters = _gridData.Filters
            };
            SetTableSortAndWhereClause(ref gridParams);
            SetFilterValues(ref gridParams);
            return gridParams;
        }

        private void SetTableSortAndWhereClause(ref GridParams gridParams)
        {
            var whereCondition = " 1=1 ";
            var searchText = gridParams.SearchText;
            switch (gridParams.Mode.ToLower())
            {
                case "mobileappusers":
                    gridParams.TableName = "MobileAppUsers";
                    whereCondition = " 1=1 ";
                    if (!string.IsNullOrEmpty(searchText))
                    {
                        whereCondition += " AND (MobileNo Like '%' + @Search + '%')";
                    }
                    break;

                case "newsticker":
                    gridParams.TableName = "NewsTicker";
                    whereCondition = " 1=1 ";
                    if (!string.IsNullOrEmpty(searchText))
                    {
                        whereCondition += " AND (Title Like '%' + @Search + '%')";
                    }
                    break;

                case "feedback":
                    gridParams.TableName = "UserFeedback u inner join MobileAppUsers m on u.CreatedBy = m.MobileAppUserId";
                    gridParams.ColumnName = " m.MobileNo, u.Title, u.Description ";
                    whereCondition = " 1=1 ";
                    if (!string.IsNullOrEmpty(searchText))
                    {
                        whereCondition += " AND (Title Like '%' + @Search + '%' OR Description Like '%' + @Search + '%'  OR MobileNo Like '%' + @Search + '%' )";
                    }
                    break;
            }
            gridParams.WhereClause = whereCondition;
        }

        private void SetFilterValues(ref GridParams gridParams)
		{
            List<string> filterWhereList = new List<string>();
            for (int i = 0; i < gridParams.Filters.Count; i++)
            {
                var filter = gridParams.Filters[i];
                var filterWhereCondition = string.Empty;
                switch (filter.Type)
                {
                    case "contains":
                        filterWhereCondition = $" '\"' + {filter.FieldName} +'\"' like '%'+{filter.FieldValue}+'%'";
                        break;
                    case "date":
                        filterWhereCondition = $" cast('\"' + {filter.FieldName} +'\"') as date) >= convert(date, {filter.FieldValue}, 103)";
                        break;                   
                    case "bool":
                        var value = filter.FieldValue == "Active" ? 1 : 0;
                        filterWhereCondition = $" {filter.FieldName} = {value}";
                        break;
                    case "int":
                        filterWhereCondition = $" {filter.FieldName} = {filter.FieldValue}";
                        break;
                    case "daterange":
                        var fieldValues = filter.FieldValue.Split(',');
                        filterWhereCondition = $" cast({filter.FieldName} as date) >= cast('{fieldValues[0].Trim()}' as date) and cast({filter.FieldName} as date) <= cast('{fieldValues[1].Trim()}' as date) ";
                        break;
                }
                filterWhereList.Add(filterWhereCondition);
            }
            if (filterWhereList.Count > 0)
            {
                var filterWhereCaluse = " ( " + string.Join(" OR ", filterWhereList) + " ) ";
                gridParams.WhereClause += " And " + filterWhereCaluse;
            }
        }
    }
}
