﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.GridOperations
{
    public class GridData
    {
        public int Page { get; set; }
        public int RecordsPerPage { get; set; }
        public string SearchText { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public string Mode { get; set; }
        public List<Filter> Filters { get; set; }

		public GridData()
		{
            Filters = new List<Filter>();
		}
    }

    public class Filter
	{
		public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public string Type { get; set; }
    }
}
