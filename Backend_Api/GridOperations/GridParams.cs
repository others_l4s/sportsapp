﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.GridOperations
{
    public class GridParams
    {
        /// <summary>
        /// Table name
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// Table name
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// Mode
        /// </summary>
        public string Mode { get; set; }

        /// <summary>
        /// Where Condition
        /// </summary>
        public string WhereClause { get; set; }

        /// <summary>
        /// default sort column
        /// </summary>
        public string SortColumn { get; set; }
        /// <summary>
        /// default sort order
        /// </summary>
        public string SortOrder { get; set; }
        /// <summary>
        /// default start number
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// default total rows per page
        /// </summary>
        public int RecordPerPage { get; set; }

        /// <summary>
        /// Search text
        /// </summary>
        public string SearchText { get; set; }

        public List<Filter> Filters { get; set; }
    }
}
