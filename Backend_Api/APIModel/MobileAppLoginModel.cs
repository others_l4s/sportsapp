﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.APIModel
{
    public class MobileAppLoginModel
	{
        public string MobileNo { get; set; }
    }

	public class MobileAppLoginModelValidator : AbstractValidator<MobileAppLoginModel>
	{
		#region ' Constructor '

		public MobileAppLoginModelValidator()
		{
			RuleFor(c => c.MobileNo).NotEmpty().WithMessage("MobileNo is required!");
		}

		#endregion ' Constructor '   
	}
}
