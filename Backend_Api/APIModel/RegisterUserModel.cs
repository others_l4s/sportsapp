﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.APIModel
{
    public class RegisterUserModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string ProfileImage { get; set; }
        public string Password { get; set; }
    }

	public class RegisterUserModelValidator : AbstractValidator<RegisterUserModel>
	{
		#region ' Constructor '

		public RegisterUserModelValidator()
		{
			RuleFor(c => c.Name).NotEmpty().WithMessage("Name is required!");
			RuleFor(c => c.ContactNo).NotEmpty().WithMessage("Contact No is required!");
			RuleFor(c => c.Email).NotEmpty().WithMessage("Email is required!");
			RuleFor(c=> c.Password).NotEmpty().WithMessage("Password is required!");
		}

		#endregion ' Constructor '   
	}
}
