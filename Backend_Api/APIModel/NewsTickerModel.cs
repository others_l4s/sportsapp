﻿using FluentValidation;

namespace SATSports.API.APIModel
{
    public class NewsTickerModel
	{
        public long NewsTickerId { get; set; }
        public string Title { get; set; }
		public bool IsActive { get; set; }
    }

	public class NewsTickerModelValidator : AbstractValidator<NewsTickerModel>
	{
		#region ' Constructor '

		public NewsTickerModelValidator()
		{
			RuleFor(c => c.Title).NotEmpty().WithMessage("Title is required!");
		}

		#endregion ' Constructor '   
	}
}
