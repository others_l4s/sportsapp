﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.APIModel
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

	public class LoginModelValidator : AbstractValidator<LoginModel>
	{
		#region ' Constructor '

		public LoginModelValidator()
		{
			RuleFor(c => c.Email).NotEmpty().WithMessage("Email is required!");
			RuleFor(c => c.Password).NotEmpty().WithMessage("Password is required!");
		}

		#endregion ' Constructor '   
	}
}
