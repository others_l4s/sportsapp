﻿using FluentValidation;

namespace SATSports.API.APIModel
{
    public class FCMTokenModel
	{
        public string MobileNo { get; set; }
        public string FCMToken { get; set; }
    }

	public class FCMTokenModelModelValidator : AbstractValidator<FCMTokenModel>
	{
		#region ' Constructor '

		public FCMTokenModelModelValidator()
		{
			RuleFor(c => c.MobileNo).NotEmpty().WithMessage("MobileNo is required!");
			RuleFor(c => c.FCMToken).NotEmpty().WithMessage("FCMToken is required!");
		}
		#endregion ' Constructor '   
	}
}
