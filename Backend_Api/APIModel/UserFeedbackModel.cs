﻿using FluentValidation;

namespace SATSports.API.APIModel
{
    public class UserFeedbackModel
	{
        public string MobileNo { get; set; }
        public string Title { get; set; }
		public string Description { get; set; }
    }

	public class UserFeedbackModelValidator : AbstractValidator<UserFeedbackModel>
	{
		#region ' Constructor '

		public UserFeedbackModelValidator()
		{
			RuleFor(c => c.Title).NotEmpty().WithMessage("Title is required!");
			RuleFor(c => c.Description).NotEmpty().WithMessage("Description is required!");
			RuleFor(c => c.MobileNo).NotEmpty().WithMessage("MobileNo is required!");
		}

		#endregion ' Constructor '   
	}
}
