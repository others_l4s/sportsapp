﻿using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.Service
{
	public class ExportService : IExportService
	{

		private readonly IWebHostEnvironment _hostingEnvironment;

		public ExportService(IWebHostEnvironment hostingEnvironment)
		{
			_hostingEnvironment = hostingEnvironment;
		}

		public string ExportFile(DataTable dt, string fileName)
		{
			try
			{
				string folderPath = "data";
				string path = Path.Combine(_hostingEnvironment.WebRootPath, folderPath);
				if (!Directory.Exists(path))
				{
					Directory.CreateDirectory(path);
				}
				string fullPath = Path.Combine(path, fileName);
				FileInfo file = new FileInfo(fullPath);
				if (file.Exists)
				{
					file.Delete();
				}
				var filepath = "";
				using (ExcelPackage excel = new ExcelPackage(file))
				{
					var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
					for (var i = 0; i < dt.Columns.Count; i++)
					{
						workSheet.Cells[1, i + 1].Value = dt.Columns[i].ColumnName.Replace("_", " ");
						workSheet.Cells[1, i + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
						workSheet.Cells[1, i + 1].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
						workSheet.Cells[1, i + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
						workSheet.Cells[1, i + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
					}

					var headerRwo = workSheet.Row(1);
					headerRwo.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 11, System.Drawing.FontStyle.Bold));

					workSheet.Row(1).Height = 30;
					int j = 2;
					foreach (DataRow obj in dt.Rows)
					{
						for (var i = 0; i < dt.Columns.Count; i++)
						{
							workSheet.Cells[j, i + 1].Value = Convert.ToString(obj[dt.Columns[i].ColumnName]);
							workSheet.Cells[j, i + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
							workSheet.Cells[j, i + 1].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
							workSheet.Cells[j, i + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
							workSheet.Cells[j, i + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
						}
						workSheet.Row(j).Height = 30;
						j++;
					}
					excel.Save();
					return "data/" + fileName;
				}
			}
			catch
			{

			}
			return string.Empty;
		}
	}
}
