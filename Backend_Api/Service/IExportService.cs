﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SATSports.API.Service
{
    public interface IExportService
    {
        string ExportFile(DataTable dt, string fileName);
    }
}
