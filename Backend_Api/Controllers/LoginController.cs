﻿using Microsoft.AspNetCore.Mvc;
using SATSports.API.APIModel;
using SATSports.API.Repository;
using SATSports.API.Utility;

namespace SATSports.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly IDatabaseRepository _databaseRepository;
        public LoginController(IDatabaseRepository databaseRepository)
        {
            _databaseRepository = databaseRepository;
        }


        [HttpPost]
        [Route("SignUp")]
        public IActionResult SignUp(MobileAppLoginModel model)
        {
            var validator = new MobileAppLoginModelValidator();
            var result = validator.Validate(model);
            if (!result.IsValid)
            {
                return StatusCode(200, result.ToApiResponse());
            }

            var userId = _databaseRepository.SaveUser(model);
            var apiResponse = new ApiResponse();
            if (userId > 0)
            {
                apiResponse.Data = userId;
                return StatusCode(200, apiResponse);
            }
            else
            {
                apiResponse.AddError("Something went wrong!");
                return StatusCode(200, apiResponse);
            }
        }
    }
}
