﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SATSports.API.APIModel;
using SATSports.API.Data;
using SATSports.API.GridOperations;
using SATSports.API.Repository;
using SATSports.API.Utility;

namespace SATSports.API.Controllers
{
    //[Authorize]
    [Route("[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly IDatabaseRepository _databaseRepository;
        private readonly ICommonRepository<NewsTickerModel> _newsTickerCommonRepo;
        private readonly ICommonRepository<UserFeedbackModel> _userFeedbackCommonRepo;
        public DataController(IDatabaseRepository databaseRepository, ICommonRepository<NewsTickerModel> newsTickerCommonRepo, ICommonRepository<UserFeedbackModel> userFeedbackCommonRepo)
        {
            _databaseRepository = databaseRepository;
            _newsTickerCommonRepo = newsTickerCommonRepo;
            _userFeedbackCommonRepo = userFeedbackCommonRepo;
        }

        [HttpPost]
        [Route("SetUserFCMToken")]
        public IActionResult SetUserFCMToken(FCMTokenModel model)
        {
            var isSaved = _databaseRepository.SaveUserFCMToken(model);
            var apiResponse = new ApiResponse();
            if (isSaved)
                return StatusCode(200, apiResponse);
            apiResponse.AddError("Not saved!");
            return StatusCode(200, apiResponse);
        }

        [HttpPost]
        [Route("SubmitFeedback")]
        public IActionResult SubmitFeedback(UserFeedbackModel model)
        {
            var validator = new UserFeedbackModelValidator();
            var result = validator.Validate(model);
            if (!result.IsValid)
            {
                return StatusCode(200, result.ToApiResponse());
            }
            _databaseRepository.SaveUserFeedback(model);
            var apiResponse = new ApiResponse();
            return StatusCode(200, apiResponse);
        }

        //For App
        [HttpGet]
        [Route("GetNewsTickers")]
        public IActionResult GetNewsTickers()
        {
            var tickers = _databaseRepository.GetNewsTickers();
            var apiResponse = new ApiResponse();
            apiResponse.Data = tickers;
            return StatusCode(200, apiResponse);
        }

        [Authorize]
        [HttpGet]
        [Route("GetAllNewsTickers")]
        public IActionResult GetAllNewsTickers([FromQuery] GridData query)
        {
            query.Mode = "newsticker";
            var data = _newsTickerCommonRepo.GetAll(query);
            var apiResponse = new ApiResponse();
            apiResponse.Data = data;
            return StatusCode(200, apiResponse);
        }

        [Authorize]
        [HttpPost]
        [Route("AddNewsTicker")]
        public IActionResult AddNewsTicker(NewsTickerModel model)
        {
            var validator = new NewsTickerModelValidator();
            var result = validator.Validate(model);
            if (!result.IsValid)
            {
                return StatusCode(200, result.ToApiResponse());
            }

            _databaseRepository.SaveNewsTicker(model);
            var apiResponse = new ApiResponse();
            return StatusCode(200, apiResponse);
        }

        [Authorize]
        [HttpPost]
        [Route("UpdateNewsTicker")]
        public IActionResult UpdateNewsTicker(NewsTickerModel model)
        {
            var validator = new NewsTickerModelValidator();
            var result = validator.Validate(model);
            if (!result.IsValid)
            {
                return StatusCode(200, result.ToApiResponse());
            }

            _databaseRepository.UpdateNewsTicker(model);
            var apiResponse = new ApiResponse();
            return StatusCode(200, apiResponse);
        }

        [Authorize]
        [HttpPost]
        [Route("DeleteNewsTicker")]
        public IActionResult DeleteNewsTicker(NewsTickerModel model)
        {
            _databaseRepository.DeleteNewsTicker(model);
            var apiResponse = new ApiResponse();
            return StatusCode(200, apiResponse);
        }

        [Authorize]
        [HttpGet]
        [Route("GetAllUserFeedBack")]
        public IActionResult GetAllUserFeedBack([FromQuery] GridData query)
        {
            query.Mode = "feedback";
            var data = _userFeedbackCommonRepo.GetAll(query);
            var apiResponse = new ApiResponse();
            apiResponse.Data = data;
            return StatusCode(200, apiResponse);
        }
    }
}
