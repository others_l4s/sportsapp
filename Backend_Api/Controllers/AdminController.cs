﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SATSports.API.APIModel;
using SATSports.API.Data;
using SATSports.API.GridOperations;
using SATSports.API.Repository;
using SATSports.API.Service;
using SATSports.API.Utility;

namespace SATSports.API.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class AdminController : ControllerBase
	{
		private readonly IDatabaseRepository _databaseRepository;
		private readonly ICommonRepository<MobileAppUsers> _userCommonRepo;
		private readonly IExportService _exportService;
		private readonly AppSettings _appSettings;
		private readonly IWebHostEnvironment _hostingEnvironment;
		public AdminController(IWebHostEnvironment hostingEnvironment, IDatabaseRepository databaseRepository, IExportService exportService, IOptions<AppSettings> options, ICommonRepository<MobileAppUsers> userCommonRepo)
		{
			_appSettings = options.Value;
			_databaseRepository = databaseRepository;
			_userCommonRepo = userCommonRepo;
			_exportService = exportService;
			_hostingEnvironment = hostingEnvironment;
		}

		[Authorize]
		[HttpGet]
		[Route("GetAllMobileAppUsers")]
		public IActionResult GetAllMobileAppUsers([FromQuery] GridData query)
		{
			var filters = Request.Query["filters"].ToString();
			if (!string.IsNullOrEmpty(filters))
			{
				query.Filters = JsonConvert.DeserializeObject<List<Filter>>(filters);
			}
			query.Mode = "mobileappusers";
			var data = _userCommonRepo.GetAll(query);
			var apiResponse = new ApiResponse();
			apiResponse.Data = data;
			return StatusCode(200, apiResponse);
		}

		[Authorize]
		[HttpGet]
		[Route("exportusers")]
		public async Task<IActionResult> ExportUsers()
		{
			var data = _userCommonRepo.GetAll();
			var filePath = _exportService.ExportFile(data, "Userlist.xlsx");
			if (!string.IsNullOrEmpty(filePath))
			{
				var path = Path.Combine(_hostingEnvironment.WebRootPath, filePath);
				var memory = new MemoryStream();
				using (var stream = new FileStream(path, FileMode.Open))
				{
					await stream.CopyToAsync(memory);
				}
				memory.Position = 0;
				return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Path.GetFileName(path));
			}
			else
			{
				return StatusCode(500, "File not exported");
			}
		}

		[HttpPost]
		[Route("Authenticate")]
		public IActionResult Authenticate(LoginModel model)
		{
			var apiResponse = new ApiResponse();
			var userId = _databaseRepository.CheckAdminLogin(model);
			if (userId > 0)
			{
				var key = Encoding.ASCII.GetBytes(_appSettings.SecretKey);
				//Generate Token for user 
				var JWToken = new JwtSecurityToken(
					issuer: _appSettings.Issuer,
					audience: _appSettings.Audience,
					claims: new Claim[]{
								new Claim(ClaimTypes.Name, model.Email)
							},
					notBefore: new DateTimeOffset(DateTime.Now).DateTime,
					expires: new DateTimeOffset(DateTime.Now.AddDays(1)).DateTime,
					signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
				);
				var token = new JwtSecurityTokenHandler().WriteToken(JWToken);
				apiResponse.Data = new { UserId = userId, Token = token, EmailId = model.Email };
				return StatusCode(200, apiResponse);
			}
			else
			{
				apiResponse.AddError("Invalid username or password!");
				return StatusCode(200, apiResponse);
			}
		}
	}
}
